!function (name, context, definition) {
    if (typeof module != 'undefined' && module.exports) module.exports = definition()
    else if (typeof define == 'function' && define.amd) define(definition)
    else context[name] = definition()
}('XhrFetch', this, function () {

    var context = this

    if ('window' in context) {
        var doc = document
            , byTag = 'getElementsByTagName'
            , head = doc[byTag]('head')[0]
    } else {
        var XHR2
        try {
            XHR2 = require('xhr2')
        } catch (ex) {
            throw new Error('Dépendance homologue `xhr2` requise ! Veuillez utiliser npm install xhr2')
        }
    }


    var schemeOfHost = /^http/,
        protocolScheme = /(^\w+):\/\//,
        tryParts = /^(20\d|1223)$/,
        readyState = 'readyState',
        contentType = 'Content-Type',
        requestedWith = 'X-Requested-With',
        uniqId = 0,
        callbackPrefix = 'xhr_fetch_' + (+new Date()),
        lastValue, // Données stockées par le rappel JSONP le plus récent
        xmlHttpRequest = 'XMLHttpRequest',
        xDomainRequest = 'XDomainRequest',
        noop = function () {
        },
        isArray = typeof Array.isArray == 'function'
            ? Array.isArray
            : function (a) {
                return a instanceof Array
            },
        defaultHeaders = {
            'contentType': 'application/x-www-form-urlencoded',
            'requestedWith': xmlHttpRequest,
            'accept': {
                '*': 'text/javascript, text/html, application/xml, text/xml, */*',
                'xml': 'application/xml, text/xml',
                'html': 'text/html',
                'text': 'text/plain',
                'json': 'application/json, text/javascript',
                'js': 'application/javascript, text/javascript'
            }
        }

        , xhr = function (o) {
            // c'est x-domain
            if (o['crossOrigin'] === true) {
                let xhr = context[xmlHttpRequest] ? new XMLHttpRequest() : null
                if (xhr && 'withCredentials' in xhr) {
                    return xhr
                } else if (context[xDomainRequest]) {
                    return new XDomainRequest()
                } else {
                    throw new Error('Le navigateur ne prend pas en charge les demandes d’origine croisée. C\'est a dire CrossOrigin')
                }
            } else if (context[xmlHttpRequest]) {
                return new XMLHttpRequest()
            } else if (XHR2) {
                return new XHR2()
            } else {
                return new ActiveXObject('Microsoft.XMLHTTP')
            }
        }
        , globalSetupOptions = {
            dataFilter: function (data) {
                return data
            }
        }

    function succeed(r) {
        let protocol = protocolScheme.exec(r.url)
        protocol = (protocol && protocol[1]) || context.location.protocol
        return schemeOfHost.test(protocol) ? tryParts.test(r.request.status) : !!r.request.response
    }

    function handleReadyState(r, success, error) {
        return function () {
            // utilisez _aborted pour atténuer l'erreur c00c023f d'IE
            // (impossible de lire les accessoires sur les objets de requête abandonnés)
            if (r._aborted) return error(r.request)
            if (r._timedOut) return error(r.request, 'La demande est abandonnée: timeout')
            if (r.request && r.request[readyState] === 4) {
                r.request.onreadystatechange = noop
                if (succeed(r)) success(r.request)
                else
                    error(r.request)
            }
        }
    }

    function setHeaders(http, o) {
        let headers = o['headers'] || {}
            , h

        headers['Accept'] = headers['Accept']
            || defaultHeaders['accept'][o['type']]
            || defaultHeaders['accept']['*']

        let isAFormData = typeof FormData !== 'undefined' && (o['data'] instanceof FormData);
        // interrompt les demandes d'origine croisée avec les navigateurs hérités
        if (!o['crossOrigin'] && !headers[requestedWith]) headers[requestedWith] = defaultHeaders['requestedWith']
        if (!headers[contentType] && !isAFormData) headers[contentType] = o['contentType'] || defaultHeaders['contentType']
        for (h in headers)
            headers.hasOwnProperty(h) && 'setRequestHeader' in http && http.setRequestHeader(h, headers[h])
    }

    function setCredentials(http, o) {
        if (typeof o['withCredentials'] !== 'undefined' && typeof http.withCredentials !== 'undefined') {
            http.withCredentials = !!o['withCredentials']
        }
    }

    function generalCallback(data) {
        lastValue = data
    }

    function urlAppend(url, s) {
        return url + (/\?/.test(url) ? '&' : '?') + s
    }

    function handleJsonp(o, fn, err, url) {
        var reqId = uniqId++,
            callbackToKey = o['jsonpCallback'] || 'callback' // La cle du 'callback' key
            ,
            callbackToVal = o['jsonpCallbackName'] || XhrFetch.getCallbackPrefix(reqId),
            callbackToRegExp = new RegExp('((^|\\?|&)' + callbackToKey + ')=([^&]+)'),
            match = url.match(callbackToRegExp),
            script = doc.createElement('script'),
            loaded = 0,
            isIE10 = navigator.userAgent.indexOf('MSIE 10.0') !== -1

        if (match) {
            if (match[3] === '?') {
                url = url.replace(callbackToRegExp, '$1=' + callbackToVal) // nom de la fonction de rappel générique
            } else {
                callbackToVal = match[3] // nom de fonction de rappel fourni
            }
        } else {
            url = urlAppend(url, callbackToKey + '=' + callbackToVal) // pas de détails de rappel, ajoutez-les
        }

        context[callbackToVal] = generalCallback

        script.type = 'text/javascript'
        script.src = url
        script.async = true
        if (typeof script.onreadystatechange !== 'undefined' && !isIE10) {
            script.htmlFor = script.id = '_xhr_fetch_' + reqId
        }

        script.onload = script.onreadystatechange = function () {
            if ((script[readyState] && script[readyState] !== 'complete' && script[readyState] !== 'loaded') || loaded) {
                return false
            }
            script.onload = script.onreadystatechange = null
            script.onclick && script.onclick()
            // Appelez le rappel de l'utilisateur avec la dernière valeur stockée et nettoyez les valeurs et les scripts.
            fn(lastValue)
            lastValue = undefined
            head.removeChild(script)
            loaded = 1
        }

        // Ajouter le script à la tête DOM
        head.appendChild(script)

        // Activer le délai d'expiration JSONP
        return {
            abort: function () {
                script.onload = script.onreadystatechange = null
                err({}, 'Request is aborted: timeout', {})
                lastValue = undefined
                head.removeChild(script)
                loaded = 1
            }
        }
    }

    function getRequest(fn, err) {
        var o = this.o
            , method = (o['method'] || 'GET').toUpperCase()
            , url = typeof o === 'string' ? o : o['url']
            // convertir les objets non-chaîne en forme de chaîne de requête sauf si o['processData'] est faux
            , data = (o['processData'] !== false && o['data'] && typeof o['data'] !== 'string')
            ? xhrFetch.toQueryString(o['data'])
            : (o['data'] || null)
            , http
            , sendWait = false

        // si nous travaillons sur une requête GET et que nous avons des données, nous devons ajouter
        // chaîne de requête à la fin de l'URL et ne pas publier de données
        if ((o['type'] == 'jsonp' || method == 'GET') && data) {
            url = urlAppend(url, data)
            data = null
        }

        if (o['type'] == 'jsonp') return handleJsonp(o, fn, err, url)

        // obtenir le xhr du factory s'il est passé
        // si la factory retourne null, retournez à l'objet couranr
        http = (o.xhr && o.xhr(o)) || xhr(o)

        http.open(method, url, o['async'] === false ? false : true)
        setHeaders(http, o)
        setCredentials(http, o)
        if (context[xDomainRequest] && http instanceof context[xDomainRequest]) {
            http.onload = fn
            http.onerror = err
            http.onprogress = function () {
            }
            sendWait = true
        } else {
            http.onreadystatechange = handleReadyState(this, fn, err)
        }
        o['before'] && o['before'](http)
        if (sendWait) {
            setTimeout(function () {
                http.send(data)
            }, 200)
        } else {
            http.send(data)
        }
        return http
    }

    function XhrFetch(o, fn) {
        this.o = o
        this.fn = fn

        init.apply(this, arguments)
    }

    function setType(header) {
        // json, javascript, text/plain, text/html, xml
        if (header === null) return undefined; //En cas d'absence de type de contenu.
        if (header.match('json')) return 'json'
        if (header.match('javascript')) return 'js'
        if (header.match('text')) return 'html'
        if (header.match('xml')) return 'xml'
    }

    function init(o, fn) {

        this.url = typeof o == 'string' ? o : o['url']
        this.timeout = null

        // si la demande a été satisfaite
        // de suivre les promesses
        this._fulfilled = false
        // success handlers
        this._successHandler = function () {
        }
        this._fulfillmentHandlers = []
        // error handlers
        this._errorHandlers = []
        // gestionnaires complets (succès et échec)
        this._completeHandlers = []
        this._erred = false
        this._responseArgs = {}

        var self = this

        fn = fn || function () {
        }

        if (o['timeout']) {
            this.timeout = setTimeout(function () {
                timedOut()
            }, o['timeout'])
        }

        if (o['success']) {
            this._successHandler = function () {
                o['success'].apply(o, arguments)
            }
        }

        if (o['error']) {
            this._errorHandlers.push(function () {
                o['error'].apply(o, arguments)
            })
        }

        if (o['complete']) {
            this._completeHandlers.push(function () {
                o['complete'].apply(o, arguments)
            })
        }

        function complete(resp) {
            o['timeout'] && clearTimeout(self.timeout)
            self.timeout = null
            while (self._completeHandlers.length > 0) {
                self._completeHandlers.shift()(resp)
            }
        }

        function success(resp) {
            let type = o['type'] || resp && setType(resp.getResponseHeader('Content-Type')) // resp peut être indéfini dans IE
            resp = (type !== 'jsonp') ? self.request : resp
            // utiliser un filtre de données global sur le texte de la réponse
            var filteredResponse = globalSetupOptions.dataFilter(resp.responseText, type)
                , r = filteredResponse
            try {
                resp.responseText = r
            } catch (e) {
                // impossible d'attribuer ceci dans IE <= 8, ignorez simplement
            }
            if (r) {
                switch (type) {
                    case 'json':
                        try {
                            resp = context.JSON ? context.JSON.parse(r) : eval('(' + r + ')')
                        } catch (err) {
                            return error(resp, 'Impossible d\'analyser JSON en réponse', err)
                        }
                        break
                    case 'js':
                        resp = eval(r)
                        break
                    case 'html':
                        resp = r
                        break
                    case 'xml':
                        resp = resp.responseXML
                        && resp.responseXML.parseError
                        && resp.responseXML.parseError.errorCode
                        && resp.responseXML.parseError.reason
                            ? null
                            : resp.responseXML
                        break
                }
            }

            self._responseArgs.resp = resp
            self._fulfilled = true
            fn(resp)
            self._successHandler(resp)
            while (self._fulfillmentHandlers.length > 0) {
                resp = self._fulfillmentHandlers.shift()(resp)
            }

            complete(resp)
        }

        function timedOut() {
            self._timedOut = true
            self.request.abort()
        }

        function error(resp, msg, t) {
            resp = self.request
            self._responseArgs.resp = resp
            self._responseArgs.msg = msg
            self._responseArgs.t = t
            self._erred = true
            while (self._errorHandlers.length > 0) {
                self._errorHandlers.shift()(resp, msg, t)
            }
            complete(resp)
        }

        this.request = getRequest.call(this, success, error)
    }

    XhrFetch.prototype = {
        abort: function () {
            this._aborted = true
            this.request.abort()
        }

        , retry: function () {
            init.call(this, this.o, this.fn)
        }
        , then: function (success, fail) {
            success = success || function () {
            }
            fail = fail || function () {
            }
            if (this._fulfilled) {
                this._responseArgs.resp = success(this._responseArgs.resp)
            } else if (this._erred) {
                fail(this._responseArgs.resp, this._responseArgs.msg, this._responseArgs.t)
            } else {
                this._fulfillmentHandlers.push(success)
                this._errorHandlers.push(fail)
            }
            return this
        }

        /**
         * `always` s'exécutera si la demande réussit ou échoue
         */
        , always: function (fn) {
            if (this._fulfilled || this._erred) {
                fn(this._responseArgs.resp)
            } else {
                this._completeHandlers.push(fn)
            }
            return this
        }

        /**
         * `fail` s'exécutera lorsque la demande échouera
         */
        , fail: function (fn) {
            if (this._erred) {
                fn(this._responseArgs.resp, this._responseArgs.msg, this._responseArgs.t)
            } else {
                this._errorHandlers.push(fn)
            }
            return this
        }
        , 'catch': function (fn) {
            return this.fail(fn)
        }
    }

    function xhrFetch(o, fn) {
        return new XhrFetch(o, fn)
    }

    // normaliser les variantes de nouvelle ligne selon les spécifications -> CRLF
    function normalize(s) {
        return s ? s.replace(/\r?\n/g, '\r\n') : ''
    }

    function serial(el, cb) {
        let n = el.name
            , t = el.tagName.toLowerCase()
            , optCb = function (o) {
            if (o && !o['disabled'])
                cb(n, normalize(o['attributes']['value'] && o['attributes']['value']['specified'] ? o['value'] : o['text']))
        }
            , ch, ra, val, i

        // ne pas sérialiser les éléments désactivés ou sans nom du formulaire
        if (el.disabled || !n) return

        switch (t) {
            case 'input':
                if (!/reset|button|image|file/i.test(el.type)) {
                    ch = /checkbox/i.test(el.type)
                    ra = /radio/i.test(el.type)
                    val = el.value
                    // WebKit nous donne "" au lieu de "on" si une case à cocher n'a pas de valeur, corrigez-la ici
                    ;(!(ch || ra) || el.checked) && cb(n, normalize(ch && val === '' ? 'on' : val))
                }
                break
            case 'textarea':
                cb(n, normalize(el.value))
                break
            case 'select':
                if (el.type.toLowerCase() === 'select-one') {
                    optCb(el.selectedIndex >= 0 ? el.options[el.selectedIndex] : null)
                } else {
                    for (i = 0; el.length && i < el.length; i++) {
                        el.options[i].selected && optCb(el.options[i])
                    }
                }
                break
        }
    }

    // collecte tous les éléments de formulaire trouvés à partir des éléments d'argument passés tous
    // le chemin vers les éléments enfants; passez un '<form>' ou des champs de formulaire.
    // appelé avec 'this' = callback à utiliser pour serial () sur chaque élément
    function eachFormElement() {
        let cb = this
            , e, i
            , serializeSubtags = function (e, tags) {
            let i, j, fa
            for (i = 0; i < tags.length; i++) {
                fa = e[byTag](tags[i])
                for (j = 0; j < fa.length; j++) serial(fa[j], cb)
            }
        }

        for (i = 0; i < arguments.length; i++) {
            e = arguments[i]
            if (/input|select|textarea/i.test(e.tagName)) serial(e, cb)
            serializeSubtags(e, ['input', 'select', 'textarea'])
        }
    }

    // sérialisation de style de chaîne de requête standard
    function serializeQueryString() {
        return xhrFetch.toQueryString(xhrFetch.serializeArray.apply(null, arguments))
    }

    // {'nom': 'valeur', ...} sérialisation de style
    function serializeHash() {
        let hash = {}
        eachFormElement.apply(function (name, value) {
            if (name in hash) {
                hash[name] && !isArray(hash[name]) && (hash[name] = [hash[name]])
                hash[name].push(value)
            } else hash[name] = value
        }, arguments)
        return hash
    }

    // [{nom: 'nom', valeur: 'valeur'}, ...] sérialisation de style
    xhrFetch.serializeArray = function () {
        let arr = []
        eachFormElement.apply(function (name, value) {
            arr.push({name: name, value: value})
        }, arguments)
        return arr
    }

    xhrFetch.serialize = function () {
        if (arguments.length === 0) return ''
        var opt, fn
            , args = Array.prototype.slice.call(arguments, 0)

        opt = args.pop()
        opt && opt.nodeType && args.push(opt) && (opt = null)
        opt && (opt = opt.type)

        if (opt == 'map') fn = serializeHash
        else if (opt == 'array') fn = xhrFetch.serializeArray
        else fn = serializeQueryString

        return fn.apply(null, args)
    }

    xhrFetch.toQueryString = function (o, trad) {
        let prefix, i
            , traditional = trad || false
            , s = []
            , enc = encodeURIComponent
            , add = function (key, value) {
            // Si value est une fonction, invoquez-la et renvoyez sa valeur
            value = ('function' === typeof value) ? value() : (value == null ? '' : value)
            s[s.length] = enc(key) + '=' + enc(value)
        }
        //Si un tableau a été transmis, supposons qu'il s'agit d'un tableau d'éléments de formulaire.
        if (isArray(o)) {
            for (i = 0; o && i < o.length; i++) add(o[i]['name'], o[i]['value'])
        } else {
            // S'il est traditionnel, encodez la méthode "ancienne"
            // fait), sinon encodez les paramètres de manière récursive.
            for (prefix in o) {
                if (o.hasOwnProperty(prefix)) buildParams(prefix, o[prefix], traditional, add)
            }
        }

        // les espaces doivent être + selon les paramètres de specencode récursivement.
        return s.join('&').replace(/%20/g, '+')
    }

    function buildParams(prefix, obj, traditional, add) {
        let name, i, v
            , rbracket = /\[\]$/

        if (isArray(obj)) {
            // Sérialiser l'élément de tableau.
            for (i = 0; obj && i < obj.length; i++) {
                v = obj[i]
                if (traditional || rbracket.test(prefix)) {
                    // Traite chaque élément du tableau comme un scalaire.
                    add(prefix, v)
                } else {
                    buildParams(prefix + '[' + (typeof v === 'object' ? i : '') + ']', v, traditional, add)
                }
            }
        } else if (obj && obj.toString() === '[object Object]') {
            // Sérialiser l'élément d'objet.
            for (name in obj) {
                buildParams(prefix + '[' + name + ']', obj[name], traditional, add)
            }

        } else {
            // Sérialise l'élément scalaire.
            add(prefix, obj)
        }
    }

    xhrFetch.getCallbackPrefix = function () {
        return callbackPrefix
    }

    // Compatibilité jQuery et Zepto, les différences peuvent être remappées ici pour que vous puissiez appeler
    // XhrFetch.compat (options, callback) d'un élément scalaire.
    xhrFetch.compat = function (o, fn) {
        if (o) {
            o['type'] && (o['method'] = o['type']) && delete o['type']
            o['dataType'] && (o['type'] = o['dataType'])
            o['jsonpCallback'] && (o['jsonpCallbackName'] = o['jsonpCallback']) && delete o['jsonpCallback']
            o['jsonp'] && (o['jsonpCallback'] = o['jsonp'])
        }
        return new XhrFetch(o, fn)
    }

    xhrFetch.ajaxSetup = function (options) {
        options = options || {}
        for (let k in options) {
            globalSetupOptions[k] = options[k]
        }
    }

    return xhrFetch
});